# Unblu cobrowsing bug repro

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts

In the project directory, you can run:

### `npm install`

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more 
### `npm install -g serve`
### `serve -s build`

## Steps to reproduce
- run project (localhost:3000)
- start cobrowsing session on web
- login to https://unblu.bankon3.eu/ as agent


## Result:
"Hello word" css styles are broken

## Cause
Unblu cobrowsing is broken for production build of styled-components library. Styled-components uses CSSOM optimization for production build and Unblu can't handle it. CSSOM Api is more performant and styles are completely hidden from the DOM tree